import sys
import pygame
import time
from PlayerTank import PlayerTank
from wall import Wall
from settings import Settings
from EnmyTank import EnmyTank
from Bullet import Bullet


class TankClash:
    def __init__(self):
        pygame.init()
        self.settings = Settings()
        self.screen = pygame.display.set_mode((self.settings.screen_width, self.settings.screen_height))
        self.bg_color = "#BDB6B3"
        self.player_tank = PlayerTank(self)
        self.walls = pygame.sprite.Group()
        self.enemy_tanks = pygame.sprite.Group()
        self.bullets = pygame.sprite.Group()
        self._fire_bullet()
        self._create_enemy_tanks()
        self._create_walls()

        pygame.display.set_caption("Tank")

    def _keydown_events(self, event):

        if event.key == pygame.K_RIGHT:
            self.player_tank.moving_right = True
            self.player_tank.moving_side = "RIGHT"
        elif event.key == pygame.K_LEFT:
            self.player_tank.moving_left = True
            self.player_tank.moving_side = "LEFT"
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()
        elif event.key == pygame.K_UP:
            self.player_tank.moving_up = True
            self.player_tank.moving_side = "UP"
        elif event.key == pygame.K_DOWN:
            self.player_tank.moving_down = True
            self.player_tank.moving_side = "DOWN"

    def _keyup_events(self, event):
        if event.key == pygame.K_RIGHT:
            self.player_tank.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.player_tank.moving_left = False
        elif event.key == pygame.K_UP:
            self.player_tank.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.player_tank.moving_down = False

    def _check_collisions(self):
        collisions = pygame.sprite.spritecollide(self.player_tank, self.walls, False)

        if (self.player_tank.moving_right and collisions and (
                self.player_tank.blocked_side is None or self.player_tank.blocked_side == 'RIGHT')):
            self.player_tank.moving_right = False
            self.player_tank.blocked_side = 'RIGHT'

        elif (self.player_tank.moving_left and collisions and (
                self.player_tank.blocked_side is None or self.player_tank.blocked_side == 'LEFT')):
            self.player_tank.moving_left = False
            self.player_tank.blocked_side = 'LEFT'

        elif (self.player_tank.moving_up and collisions and (
                self.player_tank.blocked_side is None or self.player_tank.blocked_side == 'UP')):
            self.player_tank.moving_up = False
            self.player_tank.blocked_side = 'UP'

        elif (self.player_tank.moving_down and collisions and (
                self.player_tank.blocked_side is None or self.player_tank.blocked_side == 'DOWN')):
            self.player_tank.moving_down = False
            self.player_tank.blocked_side = 'DOWN'
        else:
            self.player_tank.blocked_side = None

    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    sys.exit()
                self._keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._keyup_events(event)

    def _create_walls(self):
        available_space_x = self.settings.screen_width - 210
        number_walls_x = available_space_x // 250
        for wall_number in range(number_walls_x):
            wall = Wall(self)
            if wall_number % 2 != 0:
                wall.y = 100
                wall.rect.y = wall.y

            wall.x = 210 + 270 * wall_number
            wall.rect.x = wall.x
            self.walls.add(wall)

    def _fire_bullet(self):
        # if len(self.bullets) < self.settings.bullets_allowed:
        new_bullet = Bullet(self)
        if self.player_tank.moving_side == "UP":
            new_bullet.moving_side = "UP"
        elif self.player_tank.moving_side == "DOWN":
            new_bullet.moving_side = "DOWN"
        elif self.player_tank.moving_side == "LEFT":
            new_bullet.moving_side = "LEFT"
        elif self.player_tank.moving_side == "RIGHT":
            new_bullet.moving_side="RIGHT"
            
        self.bullets.add(new_bullet)

    def _create_enemy_tanks(self):
        n = 3
        for n in range(n):
            if n == 1:
                enemy_tank = EnmyTank(self)
                enemy_tank.y = 620
                enemy_tank.rect.y = enemy_tank.y
                enemy_tank.x = 20
                enemy_tank.rect.x = enemy_tank.x

            else:
                enemy_tank = EnmyTank(self)
                enemy_tank.y = 20 + 300 * n
                enemy_tank.rect.y = enemy_tank.y
                enemy_tank.x = 1200
                enemy_tank.rect.x = enemy_tank.x

            self.enemy_tanks.add(enemy_tank)

    def _update_bullets(self):
        b = 0
        self.bullets.update()
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0 or bullet.rect.top >= self.settings.screen_height or bullet.rect.left <= 0 or bullet.rect.right >= self.settings.screen_width:
                self.bullets.remove(bullet)
            collisions = pygame.sprite.groupcollide(self.bullets, self.walls, True, False)
            if collisions:
                self.bullets.remove(bullet)
            collisions = pygame.sprite.groupcollide(self.bullets, self.enemy_tanks, False, True)
            if collisions:
                self.bullets.remove(bullet)

    def _update_screen(self):
        self.screen.fill(self.bg_color)
        self.enemy_tanks.draw(self.screen)
        self.walls.draw(self.screen)
        # the most recent screen drawn
        # for x, y in [(200, 0), (470, 250), (740, 0), (1010, 250)]:
        #     self.draw_walls(x, y)
        for bullet in self.bullets.sprites():
            self.bullets.draw(self.screen)
        self.player_tank.blitme()
        self.player_tank.update()

        pygame.display.flip()

    def run_game(self):

        # the main loop
        while True:
            self._check_events()
            self._update_screen()
            self._check_collisions()
            self._update_bullets()


if __name__ == '__main__':
    tank = TankClash()
    tank.run_game()
