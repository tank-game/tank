import pygame
from pygame.sprite import Sprite

# EnemyTank enemy_tank
class EnmyTank(Sprite):

    def __init__(self, tankClash):
        super().__init__()
        self.screen = tankClash.screen
        self.screen_rect = tankClash.screen.get_rect()

        self.image_big_top = pygame.image.load('images/enmyTank1.png')
        self.image_big_left = pygame.image.load('images/enmyTank4.png')
        self.image_big_right = pygame.image.load('images/enmyTank2.png')
        self.image_big_down = pygame.image.load('images/enmyTank3.png')
        self.image = pygame.transform.scale(self.image_big_top, (50, 50))
        self.rect = self.image.get_rect()

        # positioning Atanks
        self.rect.x = 300
        self.rect.y = 20


        # self.x = float(self.rect.x)
        # self.y = float(self.rect.y)

    # def positioning_atanks(self, x, y):
    #     self.rect.x = x
    #     self.rect.y = y