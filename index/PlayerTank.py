import pygame
from settings import Settings
from Bullet import Bullet

class PlayerTank:
    # player tank management

    def __init__(self, tankClash):
        # initialize settings
        self.settings = Settings()

        # initialize tank position
        self.screen = tankClash.screen
        self.screen_rect = tankClash.screen.get_rect()
        #bullet

        # load the tank image and get its rect
        self.image_big_top = pygame.image.load('images/tank1.png')
        self.image_big_left = pygame.image.load('images/tank4.png')
        self.image_big_right = pygame.image.load('images/tank2.png')
        self.image_big_down = pygame.image.load('images/tank3.png')
        self.image = pygame.transform.scale(self.image_big_top, (50, 50))
        self.rect = self.image.get_rect()

        # initial position of players tank
        self.rect.x = 200
        self.rect.y = 200
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

        # movemment flag
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

        # moving side
        self.moving_side = "UP"

        # could not move side
        self.blocked_side = None

    def update(self):

        # checking if it is moving 
        if self.moving_right and self.rect.right <= self.screen_rect.right:
            self.image = pygame.transform.scale(self.image_big_right, (50, 50))
            self.x += self.settings.tank_speed
            self.rect.x = self.x
        elif self.moving_left and self.rect.left >= self.screen_rect.left:
            self.image = pygame.transform.scale(self.image_big_left, (50, 50))
            self.x -= self.settings.tank_speed
            self.rect.x = self.x
        elif self.moving_up and self.rect.top >= self.screen_rect.top:
            self.image = pygame.transform.scale(self.image_big_top, (50, 50))
            self.y -= self.settings.tank_speed
            self.rect.y = self.y
        elif self.moving_down and self.rect.bottom <= self.screen_rect.bottom:
            self.image = pygame.transform.scale(self.image_big_down, (50, 50))
            self.y += self.settings.tank_speed
            self.rect.y = self.y

    def blitme(self):
        self.screen.blit(self.image, self.rect)
