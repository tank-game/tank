import pygame
from pygame.sprite import Sprite
from settings import Settings


class Bullet(Sprite):
    def __init__(self, tankClash):

        # Create a bullet object
        super().__init__()
        self.screen = tankClash.screen
        self.settings = Settings()
        self.color = self.settings.bullet_color

        # images bullets
        self.bullet_image_big_top = pygame.image.load('images/enmyTank1.png')
        self.image = pygame.transform.scale(self.bullet_image_big_top, (8, 8))
        self.rect = self.image.get_rect()

        # Create bullet rect
        self.rect = pygame.Rect(0, 0, self.settings.bullet_width, self.settings.bullet_height)
        self.rect.center = tankClash.player_tank.rect.center
        self.y = float(self.rect.y)
        self.x = float(self.rect.x)

        # Defining bullet dirction
        self.moving_side = "TOP"

    def update(self):
        if self.moving_side == 'RIGHT':
            self.x += self.settings.bullet_speed
            self.rect.x = self.x
        elif self.moving_side == 'LEFT':
            self.x -= self.settings.bullet_speed
            self.rect.x = self.x
        elif self.moving_side == 'UP':
            self.y -= self.settings.bullet_speed
            self.rect.y = self.y
        elif self.moving_side == 'DOWN':
            self.y += self.settings.bullet_speed
            self.rect.y = self.y


    # def draw_bullet(self):
    #     pygame.draw.rect(self.screen, self.color, self.rect)

