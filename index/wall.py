import pygame
from pygame.sprite import Sprite


class Wall(Sprite):
    def __init__(self, tankClash):
        # initialize walls
        super().__init__()
        self.screen = tankClash.screen
        self.screen_rect = tankClash.screen.get_rect()

        # load wall image and get its rect
        self.wall_image_big = pygame.image.load("images/wall.png")
        self.image = pygame.transform.scale(self.wall_image_big, (60, 300))
        self.rect = self.image.get_rect()

        # positioning walls
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        # storing the exact vertically and horizontally position of walls
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

    # def positioning_walls(self, x, y):
    #     self.rect.x = x
    #     self.rect.y = y
    #
    # def blitme(self):
    #     self.screen.blit(self.wall_image, self.wall_image_rect)
