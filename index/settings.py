import pygame


class Settings:

    def __init__(self):
        self.screen_width = pygame.display.Info().current_w - 80
        self.screen_height = pygame.display.Info().current_h - 80
        self.tank_speed = 0.3

        # Bullet settings
        self.bullet_speed = 1
        self.bullet_width = 5
        self.bullet_height = 5
        self.bullet_color = '#FA0E32'
        self.bullets_allowed = 3
